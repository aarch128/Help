#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>
#define INIT_CLIENT_COUNT 0
typedef struct Info//每个客户的信息
{
	char m_count_id[11];
	char m_name[10];
	char m_id[18];
	char m_address[100];
	int m_asset;
}s_info;
typedef struct Client//总的客户
{
	int m_client_count;//客户总数
	s_info* m_client_info;//每个客户的信息
} s_client;
void Init(s_client* c);
int InputInfo(s_client * c);
void Print(s_client* c, int c_num);
void Deposit(s_client* c, int c_num);
void Withdrawal(s_client* c, int c_num);
void Pay(s_client* c, int c_num);
void Inquiry(s_client* c, int c_num);
int StrEqual(char* s1, char* s2);
int SearchSubscirpt(s_client* c, char* count_id);
void ATMMenu();
void ATM(s_client * c);