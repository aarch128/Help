#define _CRT_SECURE_NO_WARNINGS
#include"ATM.h"
#include<assert.h>
void Init(s_client* c)
{
	c->m_client_count = INIT_CLIENT_COUNT;//初始客户0个
	c->m_client_info = NULL;
}
int InputInfo(s_client*c)
{
	system("cls");
	char count_id[11]="0";
	char name[10] = "0";
	char id[19] = "0";
	char address[100] = "0";
	int asset=0;
	printf("***********持卡人信息录入***************\n");
	bool all_right = 1;
	while (all_right) {
		printf("输入账号(10位): ");
		scanf("%s", count_id);
		all_right = 0;
		for (int i = 0; i < 10; i++) {
			if (count_id[i] < '0' || count_id[i]>'9') {
				printf("\n账号格式错误，重新输入\n");
				all_right = 1;
				break;
			}
		}
		if ((SearchSubscirpt(c, count_id))>=0) {
			printf("\n卡号重复，换一个\n");
			all_right = 1;
		}
		else if ((SearchSubscirpt(c, count_id)<0)) {
			all_right = 0;
		}
	}
	printf("输入名字: ");
	while ('\0' == getchar());
	scanf("%s", name);

	all_right = 1;
	while (all_right) {
		printf("输入身份证号(18位): ");
		while ('\0' == getchar());
		scanf("%s", id);
		all_right = 0;
		for (int i = 0; i < 18; i++) {
			if (id[i] < '0' || id[i]>'9') {
				printf("\n身份证号格式错误，重新输入\n");
				all_right = 1;
				break;
			}
			else {
				continue;
			}
		}
	}
	printf("输入住址: ");
	while ('\0' == getchar());
	scanf("%s", address);

	all_right = 1;
	while (all_right) {
		printf("输入资产: ");
		scanf("%d", &asset);
		all_right = 0;
		if (asset<0) {
			printf("\n资产价值输入错误，重新输入\n");
			all_right = 1;
		}
	}
	s_info* c_tmp = (s_info*)realloc(c->m_client_info,(1 + c->m_client_count)*sizeof(s_info));
	if (NULL == c_tmp) {
		perror("calloc_m_client_info");
		assert(c_tmp);
		return -1;
	}
	else {
		c->m_client_info = c_tmp;
		c->m_client_count++;
		//free(c_tmp);
		c_tmp = NULL;
	}

	for (int i = 0; i < 11; i++) {
		c->m_client_info[c->m_client_count-1].m_count_id[i] = count_id[i];
	}
	for (int i = 0; i < 10; i++) {
		c->m_client_info[c->m_client_count-1].m_name[i] = name[i];
	}
	//memcpy(c->m_client_info[c->m_client_count - 1].m_id, id, 18);
	for (int i = 0; i < 19; i++) {
		c->m_client_info[c->m_client_count-1].m_id[i] = id[i];
	}
	for (int i = 0; i < 100; i++) {
		c->m_client_info[c->m_client_count-1].m_address[i] = address[i];
	}
	c->m_client_info[c->m_client_count-1].m_asset = asset;
	printf("信息录入成功，当前持卡人数: %d\n", c->m_client_count);
	system("pause");
	system("cls");
	return 0;
}
void Print(s_client* c,int c_subscirpt)
{
	printf("卡号：%s\n", c->m_client_info[c_subscirpt].m_count_id);
	printf("姓名：%s\n", c->m_client_info[c_subscirpt].m_name);
	printf("余额：%d\n", c->m_client_info[c_subscirpt].m_asset);
}
void Deposit(s_client* c,int c_subscirpt)
{
	Print(c, c_subscirpt);
	int deposit_money = 0;

	bool all_right = 1;
	while (all_right) {
		printf("存入：");
		scanf("%d", &deposit_money);
		all_right = 0;
		if (deposit_money < 0) {
			printf("\n存入金额输入错误，重新输入\n");
			all_right = 1;
		}
	}
	c->m_client_info[c_subscirpt].m_asset += deposit_money;
	printf("余额：%d\n", c->m_client_info[c_subscirpt].m_asset);
	system("pause");
	system("cls");
}

void Withdrawal(s_client* c, int c_subscirpt)
{
	int withdrawal_money = 0;
	bool all_right = 1;
	while (all_right) {
		printf("输入取款金额：");
		scanf("%d", &withdrawal_money);
		all_right = 0;
		if (withdrawal_money < 0) {
			printf("\n取款金额输入错误，重新输入\n");
			all_right = 1;
		}
	}
	Print(c, c_subscirpt);
	c->m_client_info[c_subscirpt].m_asset -= withdrawal_money;
	printf("现取出：%d\n最终余额：%d\n", withdrawal_money, c->m_client_info[c_subscirpt].m_asset);
	system("pause");
	system("cls");
}
void Pay(s_client* c, int c_subscirpt)
{
	int pay_money = 0;
	bool all_right = 1;
	while (all_right) {
		printf("输入消费金额：");
		scanf("%d", &pay_money);
		all_right = 0;
		if (pay_money < 0) {
			printf("\n消费金额输入错误，重新输入\n");
			all_right = 1;
		}
	}
	Print(c, c_subscirpt);
	c->m_client_info[c_subscirpt].m_asset -= pay_money;
	printf("现消费：%d\n最终余额：%d\n", pay_money, c->m_client_info[c_subscirpt].m_asset);
	system("pause");
	system("cls");
}
void Inquiry(s_client* c, int c_subscirpt)
{
	Print(c, c_subscirpt);
	system("pause");
	system("cls");
}
void Exit(s_client* c)
{
	if (c->m_client_info != NULL)
	{
		free(c->m_client_info);
		c->m_client_info = NULL;
		c->m_client_count = 0;
	}
}
void ATMMenu()
{
	printf("**************ATM***************\n");
	printf("1.存\t款\n");
	printf("2.取\t款\n");
	printf("3.购物付款\n");
	printf("4.查\t询\n");
	printf("5.退\t出\n");
	printf("输入数字[1-5]: ");

}
int StrEqual(char* s1, char* s2)
{
	for (int i = 0; i < 10; i++) {
		if (s1[i] != s2[i]) {//不相等返回0
			return 0;
		}
	}
	return 1;//相等返回1
}
int SearchSubscirpt(s_client* c, char* count_id)//查找账号对应位置
{
	if (NULL == c) {
		return -2;
	}
	for (int i = 0; i < c->m_client_count; i++) {
		if (1 == StrEqual(c->m_client_info[i].m_count_id, count_id)) {//有此账号，返回下标
			return i;
		}
	}
	return -1;//没有已存在的返回-1
}
void ATM(s_client* c)
{
	enum Options
	{
		deposit=1,
		withdrawal,
		pay,
		inquiry,
		exit
	};
	int option=1;
	int c_subscirpt = 0;
	char s_count_id[11] = "0";
	bool all_right = 1;
	while (all_right) {
		system("cls");
		printf("**************ATM***************\n");
		printf("输入操作账号(10位): ");
		scanf("%s", s_count_id);
		all_right = 0;
		for (int i = 0; i < 10; i++) {
			if (s_count_id[i] < '0' || s_count_id[i]>'9') {
				printf("\n账号格式错误，重新输入\n");
				all_right = 1;
				break;
			}
			else {
				continue;
			}
		}
		if (0 == all_right) {//格式正确，此时all_right=0
			if (SearchSubscirpt(c, s_count_id)<0) {
				printf("\n无此账号信息，重新输入\n");
				system("pause");
				all_right = 1;
			}
			else {//退出循环
				c_subscirpt = SearchSubscirpt(c, s_count_id);
				all_right = 0;
			}
		}
	}
	while (option) {
		system("cls");
		ATMMenu();
		scanf("%d", &option);
		if (option > 5 || option < 1) {
			printf("\n选择错误，重新选择\n");
			option = 1;
			continue;
		}
				
		switch (option) {
			case deposit: {
				Deposit(c, c_subscirpt);
				break;
			}
			case withdrawal: {
				Withdrawal(c, c_subscirpt);
				break;
			}
			case pay: {
				Pay(c, c_subscirpt);
				break;
			}
			case inquiry: {
				Inquiry(c, c_subscirpt);
				break;
			}
			case exit: {
				option = 0;
				printf("ATM exited\n");
				system("pause");
				break;
			}
		}
	}
}
