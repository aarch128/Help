#define _CRT_SECURE_NO_WARNINGS
#include"ATM.h"

void Menu()
{
	printf("*********************\n");
	printf("1.持卡信息录入\n");
	printf("2.ATM\n");
	printf("0.退出\n");
}
int main()
{

	enum menu_options
	{
		input_info=1,
		atm,
	};
	int menu_option = 1;
	s_client client={0,NULL};
	Init(&client);
	while (menu_option) {
		system("cls");
		Menu();
		printf("选择:>");
		scanf("%d", &menu_option);
		if (0 == menu_option || 1 == menu_option || 2 == menu_option) {
			switch (menu_option) {
				case input_info: {
					int ret = 0;
					ret=InputInfo(&client);
					if (-1 == ret) {
						return-1;
					}
					break;
				}
				case atm: {
					if (NULL == client.m_client_info) {
						printf("无存款人信息，需先录入信息\n");
						system("pause");
					}
					else {
						ATM(&client);
					}
					break;
				}
				default: {
					menu_option = 0;
					Exit(&client);
					printf("system exited\n");
					break;
				}
			}
		}
		else {
			printf("\n选择有误，重选\n");
			menu_option = 1;
		}
	}
	return 0;
}